# Mastodon share links

Put your tokens & share-links url in environment variable, put your script inside a crontab, and forget about it (it should workTM).

## Try it

```bash
git clone https://gitlab.com/sodimel/mastodon-share-links.git
cd mastodon-share-links
python3 -m venv .venv
. .venv/bin/activate
python3 -m pip install -r requirements.txt
SHARE_LINKS_INSTANCE_URL="" MASTODON_API_BASE_URL="" MASTODON_ACCESS_TOKEN="" python3 .
```

Where:

* `SHARE_LINKS_INSTANCE_URL` is the URL of your Share Link instance random url (e.g. `https://share-links.example.com/en/random/`),
* `MASTODON_API_BASE_URL` is your mastodon instance url (e.g. `https://botsin.space/`),
* `MASTODON_ACCESS_TOKEN` is your app private token −DO NOT LEAK THIS− (from settings > development > your applicatin > your access token).

Example:

```bash
SHARE_LINKS_INSTANCE_URL="https://share-links.example.com/en/random/" MASTODON_API_BASE_URL="https://botsin.space/" MASTODON_ACCESS_TOKEN="AZFY8GXW6GM-PTDSZ58JUWWWZ9MNBUYXEATVEEK49TH" python3 .
# No, that's not a real access token, I just took whatever bitwarden's password generation tool gave me.
```

## Launch it

Just put it on a crontab or idk maybe save infos in a .env file and source the .env file before running the command?

Here's my crontab entry:

```bash
$ crontab -l
20 18 * * * cd /home/corentin/services/mastodon-share-links; . .venv/bin/activate; SHARE_LINKS_INSTANCE_URL="" MASTODON_API_BASE_URL="" MASTODON_ACCESS_TOKEN="" python3 . >> log.txt
```

And here's the content of my `log.txt`:

```
$ cat log.txt
[22-06-26][SUCCESS] Posted link https:*** without error!
```