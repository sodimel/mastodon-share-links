import os
import datetime
from mastodon import Mastodon
from httplib2 import Http, ServerNotFoundError


if __name__ == "__main__":
    result_str = ""
    try:
        share_links_instance = os.getenv('SHARE_LINKS_INSTANCE_URL')

        connect = Http()
        connect.follow_redirects = False
        link = connect.request(share_links_instance)[0]['location']

        mastodon = Mastodon(
            access_token = os.getenv('MASTODON_ACCESS_TOKEN'),
            api_base_url = os.getenv('MASTODON_API_BASE_URL')
        )
        mastodon.status_post(f"Hi fediverse!\nHere's today's link:\n\n{link}")
        result_str = f"[SUCCESS] Posted link {link} without error!"
    except ServerNotFoundError:
        pass
        result_str = f"[ERROR] {share_links_instance} seems to be down, or the url is malformed, or your url is not redirecting somewhere."
    except KeyError:
        result_str = f"[ERROR] One of your env vars is not defined."

    print(f"[{datetime.datetime.now().strftime('%y-%m-%d')}]" + result_str)